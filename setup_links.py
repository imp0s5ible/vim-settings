import sys
import platform
import os


def getHomeDir():
    if platform.system() == "Windows":
        return os.environ["HOMEDRIVE"] + os.environ["HOMEPATH"]
    else:
        return os.environ["HOME"]


def getInstallDir():
    if len(sys.argv) < 2:
        ret = getHomeDir()
        print("No command line arguments, setting up in home directory ", ret)
        return ret
    else:
        print("Setting up in directory ", sys.argv[1])
        return sys.argv[1]


def getInstalledNames():
    if platform.system() == "Windows":
        return {"dotvim": "vimfiles",
                "viminfo": "_viminfo",
                "vimrc": "_vimrc"}
    else:
        return {"dotvim": ".vim",
                "viminfo": ".viminfo",
                "vimrc": ".vimrc"}

def getWindowsSymlinkFlag (is_directory):
    if is_directory:
        return 0x1
    else:
        return 0x0

def crossPlatformSymlink(src, dest, is_directory):
    if platform.system() == "Windows": 
        import ctypes
        kdll = ctypes.windll.LoadLibrary("kernel32.dll")
        kdll.CreateSymbolicLinkA(src, dest, getWindowsSymlinkFlag(is_directory))
    else:
        os.symlink(src, dest, is_directory)
        
def symlinkLog(src, dest, is_directory=False):
    print("Creating symbolic link: ", dest, " -> ", src)
    crossPlatformSymlink(src, dest, is_directory)


print("Installing symbolic links to git repo...")
install_directory = getInstallDir()
installed_names = getInstalledNames()
working_dir = os.path.dirname(os.path.realpath(__file__))
symlinkLog(working_dir + "/.vim/",
           install_directory + "/" + installed_names["dotvim"] + "/",
           True)
symlinkLog(working_dir + "/.viminfo",
           install_directory + "/" + installed_names["viminfo"])
symlinkLog(working_dir + "/.vimrc",
           install_directory + "/" + installed_names["vimrc"])
print("Installation complete!")
